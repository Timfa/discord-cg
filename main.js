const axios = require ("axios");
const minute = 60000;
const second = 1000;

var count = 0;

process.env.NODE_TLS_REJECT_UNAUTHORIZED='0'

var token = "";

axios.post("https://v3.api.cloudgarden.nl/login",
{
    username: "TBI",
    password: "J8fn3N92hH"
}).then(function(result)
{
    token = result.data.token;
    console.log(token);
    var interval = setInterval(function()
    {
        axios.get("https://v3.api.cloudgarden.nl/api/sensorLatest/ec:fa:bc:6f:c8:53",
        {
            headers:
            {
            "Authorization": "Bearer " + token.replace("JWT ", "")
            }
        }).then(function(result)
        {
            var values = result.data.results[0];
            console.log(values);

            var TVOCConcern = "Laag"

            if(values.tvoc > 300)
                TVOCConcern = "*Acceptabel*"

            if(values.tvoc > 500)
                TVOCConcern = "**Marginaal**"

            if(values.tvoc > 1000)
                TVOCConcern = "***HOOG***"

            axios.post("https://discord.com/api/webhooks/888161746542546974/dxm3GYvn-GVu-Y39-P4WA2PE0dKoRXeIs4V8fQzoT9vGywPm56tNivhiK3vWHfn1NDyc",
            {
                content: "**Temperatuur:** `" + values.temp + "°C`"
                        +"\n**Luchtvochtigheid:** `" + values.hum + "%RH`"
                        +"\n**CO₂:** `" + values.co2 + " ppm`"
                        +"\n**Vluchtige organische stof:** `" + values.tvoc + " ppb` Risico: " + TVOCConcern
                        +"\n**Fijnstof:** 1µm: `" + values.pm1 + " µg/m³`, 2.5 µg: `" + values.pm2_5 + " µg/m³`, 10µm: `" + values.pm10 + " µg/m³`"
            })

            count++;

            if(count > 6)
            {
                clearInterval(interval);
                crash();
            }
        })
    }, minute * 10)
});

